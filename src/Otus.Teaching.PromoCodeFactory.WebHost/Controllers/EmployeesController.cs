﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployee (Guid id)
        {
            try
            {
                //проверяем, может уже удален
                var employee = await _employeeRepository.GetByIdAsync(id);

                if (employee == null)
                {
                    return NotFound($"Employee with Id = {id} not found");
                }

                await _employeeRepository.DeleteAsync(employee);  
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Employee deleting error");
            }

            return Ok();
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmployee(EmployeeCreateOrUpdateRequest requestParams)
        {
            try
            {
                //дабы не плодить лишних сущностей, допустим, что проверять будем по уникальности емейла.
                var employees = await _employeeRepository.GetAllAsync();

                if (employees.Any(t => t.Email.Equals(requestParams.Email)))
                {
                    return StatusCode(StatusCodes.Status409Conflict, $"Employee with email \"{requestParams.Email}\" is already exists");
                }

                var employee = GenerateEmployeeFromRequestParams(requestParams);

                await _employeeRepository.AddAsync(employee);

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Employee creating error");
            }

            return Ok();
        }

        /// <summary>
        /// Обновить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPut ("{id:guid}")]
        public async Task<IActionResult> UpdateEmployee(Guid id,EmployeeCreateOrUpdateRequest requestParams)
        {
            try
            {
                var employees = await _employeeRepository.GetAllAsync();

                if (!employees.Any(t => t.Id == id))
                {
                    return NotFound($"Employee with Id = {id} not found");
                }
                if (employees.Any(t => t.Email.Equals(requestParams.Email) && t.Id != id))
                {
                    return StatusCode(StatusCodes.Status409Conflict, $"Employee with email \"{requestParams.Email}\" is already exists");
                }

                var employee = GenerateEmployeeFromRequestParams(requestParams, employees.FirstOrDefault(t => t.Id == id));
                await _employeeRepository.UpdateAsync(employee);

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Employee updating error");
            }

            return Ok();
        }

        private Employee GenerateEmployeeFromRequestParams(EmployeeCreateOrUpdateRequest requestParams, Employee employee = null)
        {
            if (employee == null)
            {
                employee = new Employee
                {
                    Id = Guid.NewGuid()
                };
            }

            employee.FirstName = requestParams.FirstName;
            employee.LastName = requestParams.LastName;
            employee.Email = requestParams.Email;
            employee.AppliedPromocodesCount = requestParams.AppliedPromocodesCount;

            employee.Roles.Clear();
            employee.Roles = _roleRepository.GetAllAsync().Result.
                Where(t => requestParams.RoleIds.Contains(t.Id)).ToList();
            
            return  employee;
        }
    }
}