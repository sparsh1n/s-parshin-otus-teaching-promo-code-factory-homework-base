﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task DeleteAsync(T item)
        {
            //не удаляем, а создаем новую коллекцию, где этого элемента нет
           return Task.Run( () =>
           {
               Data = Data.Where(t => t.Id != item.Id);
           });
        }

        public Task UpdateAsync(T item)
        {
            return Task.Run(() =>
            {
                var list = Data.ToList();
                var listItem = list.FirstOrDefault(t => t.Id == item.Id);
                if (listItem != null)
                {
                    listItem = item;
                }
                Data = list.AsEnumerable();
            });
        }

        public Task AddAsync(T item)
        {
            return Task.Run(() =>
            {
                var list = Data.ToList();
                
                //на всякий случай еще проверим. если в репозитории уже есть с таким Id-шником, то не добавляем!
                var listItem = list.FirstOrDefault(t => t.Id == item.Id);

                Data.FirstOrDefault(t => t.Id == item.Id);
                Data.Concat(new [] {item});

                if (listItem == null)
                {
                    list.Add(item);
                }
                //а если есть можно и заапдейтить
                //else
                //{
                //    listItem = item;
                //}

                Data = list.AsEnumerable();
            });
        }
    }
}